#Trivia-game
Created by https://gitlab.com/stianstian95 & https://gitlab.com/amalie.e
A vue project that gets questions from an api from: https://opentdb.com/api_config.php
and displays it like a website hosted on heroku here: 
https://sheltered-beyond-05528.herokuapp.com/

## About this project
The game consists of three pages, login page to create user, trivia sreen where the
questions are displayed with different questions one after the other.

## Installation
To start this on your own computer clone the project and run the following commands
in root foolder.

```
npm install
```

```
npm run dev
```

## Register user section
This is where the the user can register a name, difficulty of questions and categories.

## Trivia section
Gets the trivia questions api with the specifications chosen by the user on the previous
page. The user can not progress to the next question before the current is answered.
When the user has answered all the question it than proceeds

## Result section
This screen shows the different questions with the correct answer displayed in green
and incorrect answers shown in green. Here you can also see the highscore which updates
depending on if you beat it or not. Then you can choose to either play again(same category
and number of questions) or to return to the register screen

tester branch merge 