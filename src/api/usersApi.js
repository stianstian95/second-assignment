const apiURL = "https://noroff-triviagame2-api.herokuapp.com/trivia";
const apiKey = "kh3xUkCJ0zVlhHdSUvaAxR7vDSNKIp01i9ZtoTYE3Ekq3UW6B0t6w4H7t1FigoaQ";

// export const userInfo = {

//     /**
//      * Returns a user with the given details
//      */
//     async login(details) {
//         const requestOptions = {
//             method: "POST",
//             headers: {
//                 "X-Auth-Token": apiKey,
//                 "Content-Type": "application/json"
//             },
//             body: details
//         }
//         return fetch(apiURL, requestOptions)
//             .then(response => response.json())
//             .then(data => data.data);
//     },

//     /**
//      * Creates a new user
//      */
//     async createUser(details) {
//         const requestOptions = {
//             method: "POST",
//             headers: {
//                 "X-Auth-Token": apiKey,
//                 "Content-Type": "application/json"
//             },
//             body: details
//         }
//         return fetch(apiURL, requestOptions)
//             .then(response => response.json())
//             .then(data => data.data);
//     },

//     /**
//      * Updates user highscore
//      */
//     async updateHighscore(userId, triviaScore) {

//     }
// }

// /**
//  * checks to see if user exists
//  */
// export function userCheckExist(username) {
//     const response = fetch(`${apiURL}/trivia?username=${username}`)
//         .then(response => response.json())
//         .then(results => {
//             if (results != null) {
//                 return true
//             } else {
//                 return false
//             }
//         })
//         .catch(error => {
//             return error.message
//         })
// }


// another solution

/**
 *  method that finds user and fixes highscore/score bug
 * @param {*} details username of the user you want to get
 * @returns the user
 */
const getUser = async (details) => {
    const url = `${apiURL}?username=${details}`
    try {
        const response = await fetch(url);
        let data = await response.json();
        if (data.length == 0) {
            return ['Could not found the user', null];
        }

        let user = data[0]

        // some users have score instead of highScore
        if (!('highScore' in user)) {
            if ('score' in user) {
                user.highscore = user.score;
                delete user.score;
            } else {
                user.highScore = 0;
            }
        }
        return [null, user];
    } catch (error) {
        console.error(error);
        return [ error, null ]
    }
}

/**
 * Methods that updates the users highscore
 * @param {*} userId the Id of the user you want to update
 * @param {*} updatedHs the new highscore
 * @returns 
 */
const updateHighScore = async (userId, updatedHs) => {
    const API_DETAILS = {
        method: "PATCH",
        headers: {
            "X-API-Key": apiKey,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            highScore: updatedHs
        })
    }
    const url = `${apiURL}/${userId}`
    try {
        const response = await fetch(url, API_DETAILS);
        if (!response.ok) {
            console.error("Something went wrong updating highscore");
            return ["Somthing went wrong ...", null];
        }
        const data = await response.json();
        return [ null, data ];
    } catch(error) {
        console.error(error);
        return [ error, null ]
    }
}

/**
 *  Method that checks to see if user exist if not creates new
 * @param {*} details username of user
 * @returns the newly created user
 */
const createUser = async(details) => {
    // checks if username is taken, if not create
    const user_exist = await getUser(details);
    if (user_exist[0] == null) {
        console.error("The username you have provided already exists")
        return ['This user already exists', user_exist[1]];
    }

    const API_DETAILS = {
        method: "POST",
        headers: {
            "X-API-Key": apiKey,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            username: details,
            highScore: 0
        })
    };

    try {
        const response = await fetch(apiURL, API_DETAILS);
        if (!response.ok) {
            console.error("Something went wrong when creating user");
            return["Something went wrong...", null];
        }
        const user = await response.json();
        return [ null, user ];
    } catch(error) {
        console.error(error);
        return [ error, null ]
    }
}

export {getUser, updateHighScore, createUser};