const BASE_URL = "https://opentdb.com/"

/**
 *  function to get trivia categories
 */
async function getCategories() {

    try {
        const response = await fetch(`${BASE_URL}api_category.php`);
        const categories = await response.json();

        return [ null, categories ]

    } catch(error) {
        return [ error.message, null ]
    }
}

/**
 * function to get trivia questions
 */
async function getQuestions(triviaInfo) {

    try {
        const response = await fetch(`${BASE_URL}api.php?${triviaInfo}`);
        const data = await response.json();

        return [ null, data ]

    } catch(error) {
        return [ error.message, null ]
    }
}

/**
 * function to get trivia token
 */
async function getToken() {

    try {
        const response = await fetch(`${BASE_URL}api_token.php?command=request`);
        const token = await response.json();

        return [ null, token ]

    } catch(error) {
        return [ error.message, null ]
    }
}

/**
 * Function to reset the api token
 */
async function resetToken(token) {

    try {
        const response = await fetch(`${BASE_URL}api_token.php?command=reset&token=${token}`);
        const newToken = await response.json();

        return [ null, newToken ]

    } catch(error) {
        return [ error, null ]
    }
}

export {getCategories, getQuestions, getToken, resetToken}