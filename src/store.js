import { createStore } from "vuex";
import { getCategories, getQuestions, getToken } from "./api/questionsApi";
import { updateHighScore } from "./api/usersApi";
import he from "he";

export default createStore({
    state: {
        user: null,
        questions: null,
        token: null,
        quizParams: null,
        submittedAnswers: [],
        categories: null,
        questonIndex: 0,
        shuffledAlternatives: [],

    },
    mutations: {
        setUser: (state, payload) => {
            state.user = payload;
        },
        setCategories: (state, payload) => {
            state.categories = payload;
        },
        setQuestions: (state, payload) => {
            state.questions = payload;
        },
        setTokens: (state, payload) => {
            state.token = payload;
        },
        setQuizParams: (state, payload) => {
            payload.token = state.token;
            state.quizParams = payload;
        },
        setQuestionIndex: (state, payload) => {
            state.questionIndex = payload;
        },
        pushSubmittedAnswer: (state, payload) => {
            state.submittedAnswers.push(payload);
        },
        setHighScore: (state, payload) => {
            state.user.highScore = payload;
        },
        setSubmittedAnswers: (state, payload) => {
            state.submittedAnswers = payload;
        }
    },
    actions: {
        // fetches the questions
        async getCategories({ commit }) {
            const [errormessage, categories] =await getCategories();
            if(errormessage ===null){
                commit("setCategories", categories.trivia_categories);
            } else {
                return null;
            }
        },

        // gets question ready
        async getQuestions({ state, commit, dispatch }) {
            console.log("fetch questions");
            if (state.token) {
                const params = new URLSearchParams(state.quizParams).toString();
                const [error, questions] =await getCategories(params);
                if(error){
                    console.log(error);
                    return;
                }
                if (questions.response_code == 0) {
                    const result=[]
                    questions.results.forEach(element => {
                        element.question = he.decode(element.question)
                        element.correct_answer = he.decode(element.correct_answer)
                        element.incorrect_answers = element.incorrect_answers.map(x => he.decode(x))
                        result.push(element)
                        
                    });
                    commit("setQuestions", result);
                    commit("setQuestionIndex", 0);
                    commit("setSubmittedAnswers", []);
                    dispatch("shuffleAlternatives");
                } else {
                    console.log(questions.response_code, questions.response_message);
                }
            }
        },

        // gets api token
        async getToken({ commit }) {
            const [error,token]=await getToken();
            if(error){
                return error;
            }
            if(token.response_code==0){
                commit("setToken", token.token);
            }
            return null;
        },

        // gets current question
        iterateQuestionIndex({ state, commit }) {
            commit("setQuestionIndex", state.questionIndex +1);
        },

        // updates user highscore
        async updateHighScore({ state }) {
            let msg = await updateHighScore(state.user.id, state.user.highScore);
            if (msg[0]) {
                console.error(msg[0]);
            }
        },

        //shuffle the answers
        shuffleAlternatives({ state, commit }) {
            let arr = state.questions.map((x) => {
              let answerGroup = [...x.incorrect_answers];
              answerGroup.push(x.correct_answer);
              answerGroup.sort(() => Math.random() - 0.5);
              return answerGroup;
            });
            commit("setShuffledAlternatives", arr);
          }
      
    },

    getters: {
        // Gets the question text
        questions: (state) => {
            return state.questions.map((x) => x.question);
        },

        // Gets questions and answers!
        questionsAndAnswers: (state) => {
            let arr = [];
            for (i=0;i<state.questions.length;i++){
                let q = state.questions[i];
                const obj = {
                    index: i,
                    question: q.question,
                    correct_answer: q.correct_answer,
                    alternatives: state.shuffledAlternatives[i],
                    answer: state.submittedAnswers[i],
                };
                arr.push(obj);
            }
            return arr;
        },
    },
});