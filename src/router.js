import { createRouter, createWebHistory } from "vue-router"
import LoginPage from "./views/LoginPage.vue"
import QuestionsPage from "./views/QuestionsPage.vue"
import ResultPage from "./views/ResultPage.vue"

const routes = [
    {
        path: "/",
        component: LoginPage
    },
    {
        path: "/questionspage",
        component: QuestionsPage
    },
    {
        path: "/resultpage",
        component: ResultPage
    }
]

export default createRouter({
    history: createWebHistory(),
    routes
})